# End-To-End Framework for Data Deduplication

## About This Repository

This repository contains the source code for the Master Thesis of Floris van der Steen for his completion of the MSc
Business Analytics at the Vrije Universiteit Amsterdam. The subject of this thesis is **"Solving Data Deduplication with
the help of Deep Learning"**. This repository compares the following three deduplicator models:
  

- Cross Attribute Token Alignment Deduplicator
    - The model utilizes FastText word embeddings, a Gated Recurrent Unit layer to incorporate contextual information
      and tries to match tokens across all attributes.  
      


- Siamese Long Short-Term Memory Deduplicator
    - The model utilizes a two Long Short-Term Memory layers with shared weights and the Exponential Negative Manhattan
      Distance to compute the similarity between entity mentions.  

  
- Edit Distance Deduplicator
    - Computes entity mentions similarity by token based comparison with the Levenshtein/Hamming/Jaro-Winkler distance.  
      
  
Furthermore, all three deduplicator models use blocking rules to cope with the volume complexity of big data and
hierarchical clustering to compute the cluster identities of the entity mentions.

Data used for this research:

- Elementary School dataset (Link)
- Music Brainz (Link)
- North Carolina Voters (Link)

The following data format is an example of a format that is suitable for the deduplicator models:

|   index | site_name                                                      | address                      | zip   |   phone |    true_id |
|--------:|:---------------------------------------------------------------|:-----------------------------|:------|--------:|-----------:|
|    2246 | metropolitan family services midway head start                 | 6422 s. kedzie               | 60629 | 7374790 |  642260629 |
|     621 | herbert                                                        | 2131 w. monroe               |       | 5347806 |  213160612 |
|    2168 | firman community services firman west ii sa 4644 s. dearborn   | 4644 s. dearborn             | 60609 | 3733400 |  464460609 |
|    1339 | east moline citizens for comm. center                          | 489-27th street, east moline | 61244 | 7555031 | 4892761244 |
|     569 | peabody                                                        | 1444 w. augusta              |       | 5344170 |  144460622 |
|    3302 | delano                                                         | 3905 w wilcox street         | 60624 | 5346450 |  390560624 |
|     880 | wheatley cpc                                                   | 902 e. 133rd pl              |       | 5355718 |   90260627 |
|    1494 | chicago public schools deneen, charles s.                      | 7240 s wabash ave            | 60619 | 5353035 |  724060619 |
|    1438 | catholic charities of the archdiocese of chicago grace mission | 5332 s western               | 60609 | 4761990 |  533260609 |
|    1832 | metropolitan family services midway head start                 | 6422 s kedzie                | 60629 | 7374790 |  642260629 |

It is important that the dataset contains a column "index" representing the unique id of a data record and a column
"true_id" containing the true label of a data record. Furthermore, the user can specify which attributes should be used
by the models, in the example this can be "site_name", "address", "zip" and "phone".

## Repository Map

```
ThesisBA
├── README.md
├── requirements.txt
├── setup.py
│
├── notebooks
│   ├── Example
│   │    └── all_models.ipynb
│   └── Thesis
│        ├── Data1.ipynb
│        ├── Data2.ipynb    
│        └── Data3.ipynb
│
├── deduplication
│   ├── base
│   │     ├── model.py
│   │     └── transformer.py
│   ├── dataprocessing
│   │     └── data_processing.py
│   ├── deduplicator
│   │     ├── cata_deduplicator.py
│   │     ├── siamese_deduplicator.py
│   │     └── rule_based_deduplicator.py
│   ├── blocking
│   │     ├── blocking_rules.py
│   │     ├── completeness_blocker.py
│   │     └── train_pair_creator.py
│   ├── similarity
│   │     ├── cross_attribute_token_alignment_similarity.py
│   │     ├── siamese_lstm_similarity.py
│   │     └── edit_distance_similarity.py
│   ├── clustering
│   │     └── hierarchical_clustering.py
│   ├── evaluating
│   │     └── evaluating.py
│   │
│   ├── config.py
│   └── project_utils.py

├── embedding
└── savedmodels

```

## Usage

The deduplicator models can be used separately as they are all written in their own class. An example on how to call
these algorithms can be found in **Notebooks/Example/all_models.ipynb**. The required Python libraries for this repository
can be found in **requirements.txt**.

