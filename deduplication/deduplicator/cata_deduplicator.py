import datetime as dt
import numpy as np
import os
import fasttext
from deduplication.base.model import Model
from deduplication.blocking.train_pair_creator import TrainPairsCreator
from deduplication.blocking.completeness_blocker import CompletenessBlocker
from deduplication.config import EMBEDDING_PATH
from deduplication.config import SAVED_MODELS_PATH
from deduplication.similarity.cross_attribute_token_alignment_similarity import CrossAttributeTokenAlignment
from deduplication.clustering.hierarchical_clustering import EntityClustering


class CataDeduplicator(Model):
    """
    Deduplicator that uses a Cross-attribute Token Alignment model to generate similarity scores.
    :param attribute_names: Specifies the names of the attributes.
    :param amount_attribute_tokens: The number of tokens uses for each attribute. If not specified all attribute will
                                    use 6 tokens.
    :param blocking_rules: specifies the blocking rules that are going to be used. If 'all' it will select all blocking
                           rules that are available.
    :param train_ratio: Indicates the ratio between matches and non-matches in the train and validation pairs.
                        e.g. 1 => (match: 50% / non-match: 50%), 2 => (match: 33% / non-match: 67%).
    :param completeness_threshold: The minimum completeness value that the produced blocks need to meet.
    :param complete: True => returns all possible pairs.
    :param input_length: Size of embedded input from Fasttext. Default = 300.
    :param gru_dim_size: Dimension of the output space of the GRU layer. Default = 50.
    :param dense_dim_size: Dimension of the output space of the Dense layers. Default = 100.
    :param batch_size: Number of samples per gradient update. Default = 32.
    :param n_epochs: Number of epochs to train the model. Default = 500.
    :param learning_rate: Learning rate for the Adam optimizer. Default = 0.001.
    :param patience: Number of epochs with no improvement after which training will be stopped. Default = 50.
    :param load_trained_model: True => model is trained from scratch, False => an earlier trained model is loaded
    :param weak_link_threshold: If a similarity score is below this threshold, the pair is discarded and not considered
                                as a match. Default = 0.05.
    :param cut_threshold: The threshold to apply when forming flat clusters (cluster id's). Default = 0.5.
    :param embedding_path: Path to the location of the Fasttext embedding model
    :param saved_model_path: Path to the location where models are saved and loaded.
    :param verbose: The level of logging information.
    """

    def __init__(self, attribute_names, amount_attribute_tokens=None, blocking_rules='all', train_ratio=1,
                 completeness_threshold=1, complete=False, input_length=300, gru_dim_size=50, dense_dim_size=100,
                 batch_size=32, n_epochs=500, learning_rate=0.001, patience=50, load_trained_model=False,
                 weak_link_threshold=0.05, cut_threshold=0.5, embedding_path=EMBEDDING_PATH,
                 saved_model_path=SAVED_MODELS_PATH, verbose=1):

        self.attribute_names = attribute_names
        self.number_of_attributes = len(self.attribute_names)

        # blocker parameters
        self.blocking_rules = blocking_rules
        self.train_ratio = train_ratio
        self.completeness_threshold = completeness_threshold
        self.complete = complete
        super().__init__()

        # similarity parameters
        if amount_attribute_tokens is None:
            self.amount_attribute_tokens = [6 for _ in range(self.number_of_attributes)]
        else:
            self.amount_attribute_tokens = amount_attribute_tokens
        self.input_length = input_length
        self.gru_dim_size = gru_dim_size
        self.dense_dim_size = dense_dim_size
        self.batch_size = batch_size
        self.n_epochs = n_epochs
        self.learning_rate = learning_rate
        self.patience = patience
        self.load_trained_model = load_trained_model

        # clustering parameters
        self.weak_link_threshold = weak_link_threshold
        self.cut_threshold = cut_threshold

        self.embedding_path = embedding_path
        self.saved_model_path = saved_model_path
        self.verbose = verbose

        self.Blocker = CompletenessBlocker(self.attribute_names, self.blocking_rules, self.completeness_threshold)
        self.CreateTrainPairs = TrainPairsCreator(self.train_ratio, self.complete)
        self.CATAModel = CrossAttributeTokenAlignment(self.attribute_names, self.amount_attribute_tokens,
                                                      self.input_length, self.gru_dim_size, self.dense_dim_size,
                                                      self.batch_size, self.n_epochs, self.learning_rate, self.patience)

        if load_trained_model and not os.path.exists(os.path.join(self.saved_model_path, self.CATAModel.name)):
            raise FileNotFoundError("There does not exist such a model. Please set load_trained_model to False.")

        self.start_fit = self.end_fit = self.start_predict = self.end_predict = None

        super().__init__()

    def fit(self, x_train, x_val=None):
        """
        Fits the deduplicator. I.e. pick the appropriate blocking rules and fits the Cross-attribute Token Alignment
        model for similarity scoring.
        :param x_train: train set.
        :param x_val: validation set.
        return self
        """

        if self.verbose > 0:
            print("Start fitting deduplicator model")
            self.start_fit = dt.datetime.now()

        ft = fasttext.load_model(self.embedding_path)

        training_entity_mentions = x_train.copy()
        training_entity_mentions = self._create_fasttext_embeddings(ft, training_entity_mentions)

        if x_val is not None:
            validation_entity_mentions = x_val.copy()
            validation_entity_mentions = self._create_fasttext_embeddings(ft, validation_entity_mentions)
        del ft

        if self.verbose > 0:
            print("Token embedding done")

        self.Blocker.fit(training_entity_mentions)
        if self.verbose > 0:
            print("Blocking rules:")
            print(f"First rule: {self.Blocker.rule_1[0]} of {self.Blocker.rule_1[1]}")
            print(f"Second rule: {self.Blocker.rule_2[0]} of {self.Blocker.rule_2[1]}")

        training_pairs = self.CreateTrainPairs.transform(training_entity_mentions)
        if x_val is not None:
            validation_pairs = self.CreateTrainPairs.transform(validation_entity_mentions)
        else:
            validation_pairs = []

        if self.verbose > 0:
            print("Starting fitting Cross Attribute Token Alignment similarity model")
            print(f"Number of training pairs: {len(training_pairs)}")
            print(f"Number of validation pairs: {len(validation_pairs)}")

        if self.load_trained_model:
            self.CATAModel.load_model(self.saved_model_path)
            if self.verbose > 0:
                print("Loading model done")
        else:
            self.CATAModel.fit(x_train=training_pairs, x_val=validation_pairs)
            self.CATAModel.save_model(self.saved_model_path)

        if self.verbose > 0:
            print("Done fitting deduplicator model")
            self.end_fit = dt.datetime.now()
            print(f"Fitting deduplicator model took: {self.end_fit - self.start_fit}")

        return self

    def predict(self, x):
        """"
        Predicts the cluster id's of the provided test set. I.e. use the blocking rules to define the comparison pairs,
        computes the similarity scores for these comparison pairs and utilizes hierarchical clustering to produce the
        cluster id's.
        :param x: test set
        :return Pandas DataFrame, (index, clusterID)
        """

        if self.verbose > 0:
            print("Start predicting")
            self.start_predict = dt.datetime.now()

        test_entity_mentions = x.copy()
        ft = fasttext.load_model(self.embedding_path)
        test_entity_mentions = self._create_fasttext_embeddings(ft, test_entity_mentions)
        del ft

        if self.verbose > 0:
            print('Token embedding done')

        test_pairs = self.Blocker.transform(test_entity_mentions)
        if self.verbose > 0:
            print(f"Amount of pairwise comparisons: {len(test_pairs)}")

        similarity_scores = self.CATAModel.predict(test_pairs)

        indices = test_entity_mentions['index'].to_numpy()
        hierarchical_clusters = EntityClustering(indices)
        df_clusters = hierarchical_clusters.transform(similarity_scores)

        if self.verbose > 0:
            print("Done predicting clusters with deduplicator model")
            self.end_predict = dt.datetime.now()
            print(f"Predicting took: {self.end_predict - self.start_predict}")

        return df_clusters

    def _create_fasttext_embeddings(self, ft, df):
        """
        Constructs fasttext token embeddings for a dataset
        :param ft: fasttext model
        :param df: dataset that needs embeddings
        """
        for attribute, max_token_count in zip(self.attribute_names, self.amount_attribute_tokens):
            df['embedded_' + attribute] = [attribute_string_to_list_of_embeddings(ft, string, max_token_count) for
                                           string in df[attribute]]
        return df


def attribute_string_to_list_of_embeddings(ft, string, max_token_count):
    """
    Constructs Fasttext token embeddings for an attribute string. The embedding vector is padded or truncated to achieve
    the desired amount of tokens.
    :param ft: Fasttext model
    :param string: Attribute that needs embeddings
    :param max_token_count: Number of tokens that need embeddings
    :return ndarray, with length max_token_count containing lists of 300 values corresponding to the token embeddings
    """
    embeddings_vectors = [ft.get_word_vector(token) for token in string.split(" ") if len(token) > 1][:max_token_count]
    while len(embeddings_vectors) < max_token_count:
        embeddings_vectors.append(100 * np.random.rand(300))
    return np.array(embeddings_vectors)
