import datetime as dt
from deduplication.blocking.completeness_blocker import CompletenessBlocker
from deduplication.similarity.edit_distance_similarity import EditDistanceSimilarity
from deduplication.clustering.hierarchical_clustering import EntityClustering
from deduplication.base.model import Model


class RuleBasedDeduplicator(Model):
    """
    Deduplicator that uses a edit distance (string metric) model to generate similarity scores.
    :param attribute_names: Specifies the names of the attributes.
    :param blocking_rules: specifies the blocking rules that are going to be used. If 'all' it will select all blocking
                           rules that are available.
    :param completeness_threshold: The minimum completeness value that the produced blocks need to meet.
    :param complete: True => returns all possible pairs.
    :param similarity_algorithm: Algorithm used to determine the similarity between token strings between entity record
                                 mentions. Default = 'Jaro-Winkler'.
    :param similarity_threshold: Threshold dat determines whether or not token strings are considered to be similar.
                                 Default = 0.95.
    :param weak_link_threshold: If a similarity score is below this threshold, the pair is discarded and not considered
                                as a match. Default = 0.05.
    :param cut_threshold: The threshold to apply when forming flat clusters (cluster id's). Default = 0.5.
    :param verbose: The level of logging information.
    """

    def __init__(self, attribute_names, blocking_rules='all', completeness_threshold=1, complete=False,
                 similarity_algorithm='Jaro-Winkler', similarity_threshold=0.95, weak_link_threshold=0.05,
                 cut_threshold=0.5, verbose=1):

        self.attribute_names = attribute_names

        # blocking parameters
        self.blocking_rules = blocking_rules
        self.completeness_threshold = completeness_threshold
        self.complete = complete

        # similarity parameters
        self.similarity_algorithm = similarity_algorithm
        self.similarity_threshold = similarity_threshold

        # clustering parameters
        self.weak_link_threshold = weak_link_threshold
        self.cut_threshold = cut_threshold

        self.verbose = verbose

        self.Blocker = CompletenessBlocker(self.attribute_names, self.blocking_rules, self.completeness_threshold)
        self.RLModel = EditDistanceSimilarity(self.attribute_names, self.similarity_algorithm,
                                              self.similarity_threshold)

        self.start_fit = self.end_fit = self.start_predict = self.end_predict = None

        super().__init__()

    def fit(self, x_train, x_val=None):
        """
        Fits the deduplicator. I.e. pick the appropriate blocking rules.
        :param x_train: train set.
        :param x_val: validation set.
        return self
        """
        if self.verbose > 0:
            print("Start fitting deduplicator model")
            self.start_fit = dt.datetime.now()
        training_entity_mentions = x_train.copy()
        self.Blocker.fit(training_entity_mentions)
        if self.verbose > 0:
            print("Blocking rules:")
            print(f"First rule: {self.Blocker.rule_1[0]} of {self.Blocker.rule_1[1]}")
            print(f"Second rule: {self.Blocker.rule_2[0]} of {self.Blocker.rule_2[1]}")

        if self.verbose > 0:
            print("Done fitting deduplicator model")
            self.end_fit = dt.datetime.now()
            print(f"Fitting deduplicator model took: {self.end_fit - self.start_fit}")
        return self

    def predict(self, x):
        """"
        Predicts the cluster id's of the provided test set. I.e. use the blocking rules to define the comparison pairs,
        computes the similarity scores for these comparison pairs and utilizes hierarchical clustering to produce the
        cluster id's.
        :param x: test set
        :return Pandas DataFrame, (index, clusterID)
        """
        if self.verbose > 0:
            print("Start predicting")
            self.start_predict = dt.datetime.now()

        test_pairs = self.Blocker.transform(x)
        if self.verbose > 0:
            print(f"Amount of pairwise comparisons: {len(test_pairs)}")

        similarity_scores = self.RLModel.transform(test_pairs)

        indices = x['index'].to_numpy()
        hierarchical_clusters = EntityClustering(indices)
        df_clusters = hierarchical_clusters.transform(similarity_scores)

        if self.verbose > 0:
            print("Done predicting clusters with deduplicator model")
            self.end_predict = dt.datetime.now()
            print(f"Predicting took: {self.end_predict - self.start_predict}")
        return df_clusters
