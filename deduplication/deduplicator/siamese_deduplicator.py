import string
import os
import datetime as dt

from deduplication.base.model import Model
from deduplication.blocking.train_pair_creator import TrainPairsCreator
from deduplication.blocking.completeness_blocker import CompletenessBlocker

from deduplication.config import EMBEDDING_PATH
from deduplication.config import SAVED_MODELS_PATH
from deduplication.similarity.siamese_lstm_similarity import SiameseLSTM
from deduplication.clustering.hierarchical_clustering import EntityClustering


class SiameseDeduplicator(Model):
    """
    Deduplicator that uses a siamese lstm model to generate similarity scores.
    :param attribute_names: Specifies the names of the attributes.
    :param blocking_rules: specifies the blocking rules that are going to be used. If 'all' it will select all blocking
                           rules that are available.
    :param train_ratio: Indicates the ratio between matches and non-matches in the train and validation pairs.
                        e.g. 1 => (match: 50% / non-match: 50%), 2 => (match: 33% / non-match: 67%).
    :param completeness_threshold: The minimum completeness value that the produced blocks need to meet.
    :param complete: True => returns all possible pairs.
    :param input_size: The amount of characters from the data string that are going to be used in the model.
                        Default = 125.
    :param emb_dim: Dimension of the output space of the embedding layer. Default = 24.
    :param lstm_dim: Dimension of the output space of the LSTM layer. Default = 100.
    :param batch_size: Number of samples per gradient update. Default = 32.
    :param n_epochs: Number of epochs to train the model. Default = 500.
    :param learning_rate: Learning rate for the Adam optimizer. Default = 0.001.
    :param patience: Number of epochs with no improvement after which training will be stopped. Default = 50.
    :param vocabulary: Text corpus used for building the character dictionary. Default = all known characters defined
                       by string.printable.
    :param load_trained_model: True => model is trained from scratch, False => an earlier trained model is loaded
    :param weak_link_threshold: If a similarity score is below this threshold, the pair is discarded and not considered
                                as a match. Default = 0.05.
    :param cut_threshold: The threshold to apply when forming flat clusters (cluster id's). Default = 0.5.
    :param embedding_path: Path to the location of the Fasttext embedding model
    :param saved_model_path: Path to the location where models are saved and loaded.
    :param verbose: The level of logging information.
    """

    def __init__(self, attribute_names, blocking_rules='all', train_ratio=1, completeness_threshold=1, complete=False,
                 input_size=125, emb_dim=24, lstm_dim=100, batch_size=32, n_epochs=500, learning_rate=0.001,
                 patience=50, vocabulary=string.printable, load_trained_model=False, weak_link_threshold=0.05,
                 cut_threshold=0.5, embedding_path=EMBEDDING_PATH, saved_model_path=SAVED_MODELS_PATH, verbose=1):

        self.attribute_names = attribute_names

        # blocker parameters
        self.blocking_rules = blocking_rules
        self.train_ratio = train_ratio
        self.completeness_threshold = completeness_threshold
        self.complete = complete

        # similarity parameters
        self.input_size = input_size
        self.emb_dim = emb_dim
        self.lstm_dim = lstm_dim
        self.batch_size = batch_size
        self.n_epochs = n_epochs
        self.learning_rate = learning_rate
        self.patience = patience
        self.load_trained_model = load_trained_model
        self.vocabulary = vocabulary

        # clustering parameters
        self.weak_link_threshold = weak_link_threshold
        self.cut_threshold = cut_threshold

        self.embedding_path = embedding_path
        self.saved_model_path = saved_model_path
        self.verbose = verbose

        self.Blocker = CompletenessBlocker(self.attribute_names, self.blocking_rules, self.completeness_threshold)
        self.CreateTrainPairs = TrainPairsCreator(self.train_ratio, self.complete)
        self.LSTMModel = SiameseLSTM(self.attribute_names, self.input_size, self.emb_dim, self.lstm_dim,
                                     self.batch_size, self.n_epochs, self.learning_rate, self.patience, self.vocabulary)

        if load_trained_model and not os.path.exists(os.path.join(self.saved_model_path, self.LSTMModel.name)):
            raise FileNotFoundError("There does not exist such a model. Please set load_trained_model to False.")

        self.start_fit = self.end_fit = self.start_predict = self.end_predict = None

        super().__init__()

    def fit(self, x_train, x_val=None):
        """
        Fits the deduplicator. I.e. pick the appropriate blocking rules and fits the Siamese LSTM model for similarity
        scoring.
        :param x_train: train set.
        :param x_val: validation set.
        return self
        """
        if self.verbose > 0:
            print("Start fitting deduplicator model")
            self.start_fit = dt.datetime.now()
        training_entity_mentions = x_train.copy()
        self.Blocker.fit(training_entity_mentions)
        if self.verbose > 0:
            print("Blocking rules:")
            print(f"First rule: {self.Blocker.rule_1[0]} of {self.Blocker.rule_1[1]}")
            print(f"Second rule: {self.Blocker.rule_2[0]} of {self.Blocker.rule_2[1]}")

        training_pairs = self.CreateTrainPairs.transform(training_entity_mentions)
        if x_val is not None:
            validation_entity_mentions = x_val.copy()
            validation_pairs = self.CreateTrainPairs.transform(validation_entity_mentions)
        else:
            validation_pairs = []

        if self.verbose > 0:
            print("Starting fitting Cross Attribute Token Alignment similarity model")
            print(f"Number of training pairs: {len(training_pairs)}")
            print(f"Number of validation pairs: {len(validation_pairs)}")

        if self.load_trained_model:
            self.LSTMModel.load_model(self.saved_model_path)
            if self.verbose > 0:
                print("Loading model done")
        else:
            self.LSTMModel.fit(x_train=training_pairs, x_val=validation_pairs)
            self.LSTMModel.save_model(self.saved_model_path)

        if self.verbose > 0:
            print("Done fitting deduplicator model")
            self.end_fit = dt.datetime.now()
            print(f"Fitting deduplicator model took: {self.end_fit - self.start_fit}")

        return self

    def predict(self, x):
        """"
        Predicts the cluster id's of the provided test set. I.e. use the blocking rules to define the comparison pairs,
        computes the similarity scores for these comparison pairs and utilizes hierarchical clustering to produce the
        cluster id's.
        :param x: test set
        :return Pandas DataFrame, (index, clusterID)
        """

        if self.verbose > 0:
            print("Start predicting")
            self.start_predict = dt.datetime.now()

        test_pairs = self.Blocker.transform(x)
        if self.verbose > 0:
            print(f"Amount of pairwise comparisons: {len(test_pairs)}")

        similarity_scores = self.LSTMModel.predict(test_pairs)

        indices = x['index'].to_numpy()
        hierarchical_clusters = EntityClustering(indices)
        df_clusters = hierarchical_clusters.transform(similarity_scores)

        if self.verbose > 0:
            print("Done predicting clusters with deduplicator model")
            self.end_predict = dt.datetime.now()
            print(f"Predicting took: {self.end_predict - self.start_predict}")
        return df_clusters
