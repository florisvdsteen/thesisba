import abc


class Model:
    """
    Base class Model with abstract methods fit and predict, similar meaning as in scikit-learn
    """
    def __init__(self):
        pass

    @abc.abstractmethod
    def fit(self, x_train, x_val=None):
        pass

    @abc.abstractmethod
    def predict(self, x):
        pass

    def fit_predict(self, x_train, x_val=None):
        return self.fit(x_train, x_val).predict(x_train)
