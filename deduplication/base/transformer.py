import abc


class Transformer:
    """
    Base class transformer with abstract methods fit and transform, similar meaning as in scikit-learn.
    """
    def __init__(self):
        pass

    @abc.abstractmethod
    def fit(self, x):
        pass

    @abc.abstractmethod
    def transform(self, x):
        pass

    def fit_transform(self, x):
        return self.fit(x).transform(x)
