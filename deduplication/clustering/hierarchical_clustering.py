from deduplication.base.transformer import Transformer
import networkx as nx
from scipy.cluster.hierarchy import linkage, fcluster
import pandas as pd
import scipy.spatial.distance as ssd


class EntityClustering(Transformer):
    """
    Computes clusters based on the similarity scores for the selected pairs of the test set. This is done with the
    hierarchical clustering algorithm.
    :param indices: list with all indices in the test set
    :param weak_link_threshold: If a similarity score is below this threshold, the pair is discarded and not considered
                                as a match. Default = 0.05.
    :param cut_threshold: The threshold to apply when forming flat clusters (cluster id's). Default = 0.5.
    """

    def __init__(self, indices, weak_link_threshold=0.05, cut_threshold=0.5):
        self.weak_link_threshold = weak_link_threshold
        self.cut_threshold = cut_threshold
        self.indices = indices

        super().__init__()

    def fit(self, x):
        return self

    def transform(self, x):
        """
        First the model applies the connected components algorithm to split the entity mentions into components. Then
        for each component a complete graph (Nodes = entity mentions, Edges = similarity scores) is constructed.
        Additionally the model uses hierarchical clustering with centroid linkage to compute clusters. If a components
        only contains one entity mention this mention gets an unique cluster id.
        :param x: the selected pairs and their similarity scores
        :return Pandas DataFrame, (index, clusterID)
        """
        graph = nx.Graph()
        for index_1, index_2, sim_score in zip(x['index_1_index'], x['index_2_index'], x['similarity_score']):
            if sim_score > self.weak_link_threshold:
                graph.add_edge(index_1, index_2, weight=1 - sim_score)

        components = nx.connected_components(graph)
        cluster_counter = 0
        clustering = {}

        for component in components:
            complete_graph = nx.complete_graph(component)
            nx.set_edge_attributes(complete_graph, values=1, name='weight')
            complete_graph = nx.compose(complete_graph, graph.subgraph(component))
            cond_dist_matrix = ssd.squareform(nx.convert_matrix.to_numpy_matrix(complete_graph))
            z = linkage(cond_dist_matrix, 'centroid')
            clustering.update(dict(
                zip(complete_graph.nodes(), fcluster(z, t=self.cut_threshold, criterion='distance') + cluster_counter)))
            cluster_counter += len(component)

        for idx in self.indices:
            if idx not in clustering:
                clustering[idx] = cluster_counter
                cluster_counter += 1

        df_clusters = pd.DataFrame(list(clustering.items()), columns=['index', 'cluster_id'])
        return df_clusters
