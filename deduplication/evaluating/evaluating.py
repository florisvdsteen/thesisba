import pandas as pd
from sklearn.metrics import adjusted_rand_score, homogeneity_completeness_v_measure

from sklearn.metrics import accuracy_score,precision_score,recall_score


def number_of_combinations(size):
    return 0.5 * size * (size - 1)


def calculate_false_negatives(group):
    pairs_possible = number_of_combinations(len(group[1]))
    pairs_within = sum(map(number_of_combinations, group[1].groupby('cluster_id').count()['index']))
    return pairs_possible - pairs_within


def compute_metrics(df_results):
    total_pairs = number_of_combinations(len(df_results))

    total_positives = sum(map(number_of_combinations, df_results.groupby('cluster_id').count()['index']))
    true_positives = sum(map(number_of_combinations, df_results.groupby(['cluster_id', 'true_id']).count()['index']))
    false_positives = total_positives - true_positives

    total_negatives = total_pairs - total_positives
    false_negatives = sum(map(calculate_false_negatives, df_results.groupby(['true_id'])))
    true_negatives = total_negatives - false_negatives

    if total_positives == 0:
        precision = 1
    else:
        precision = true_positives / (true_positives + false_positives)
    if true_positives + false_negatives == 0:
        recall = 1
    else:
        recall = true_positives / (true_positives + false_negatives)
    f1score = 2 * ((precision * recall) / (precision + recall))
    accuracy = (true_positives + true_negatives) / total_pairs

    return precision, recall, f1score, accuracy, true_positives, false_positives, false_negatives, true_negatives


def score(model_name, data_set, df_clusters):
    df_results = pd.merge(df_clusters, data_set[['index', 'true_id']], on="index")
    precision, recall, f1score, accuracy, true_positives, false_positives, false_negatives, true_negatives \
        = compute_metrics(df_results)
    homogeneity, completeness, v_measure = homogeneity_completeness_v_measure(df_results['true_id'],
                                                                              df_results['cluster_id'])
    results = {'Number of clusters': df_results['cluster_id'].nunique(),
               'Adjusted Rand Index': adjusted_rand_score(df_results['true_id'], df_results['cluster_id']),
               'Homogeneity': homogeneity,
               'completeness': completeness,
               'v_measure': v_measure,
               'Accuracy': accuracy,
               'Precision': precision,
               'Recall': recall,
               'F1score': f1score,
               'True positives': true_positives,
               'False positives': false_positives,
               'False negatives': false_negatives,
               'True negatives': true_negatives
               }
    df_metrics = pd.DataFrame(results, index=[model_name])
    return df_metrics


def score_matching_methods(model_name,similarity_scores, match_threshold = 0.5):
    similarity_scores['match'] = (similarity_scores['similarity_score'] > match_threshold)
    
    results = {'Accuracy': accuracy_score(similarity_scores['label'],similarity_scores['match']),
               'Precision': precision_score(similarity_scores['label'],similarity_scores['match']),
               'Recall': recall_score(similarity_scores['label'],similarity_scores['match'])
               }

    df_metrics = pd.DataFrame(results,index=[model_name])
    return df_metrics
