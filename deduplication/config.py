import os

from deduplication.project_utils import get_project_root

EMBEDDING_PATH = os.path.join(get_project_root(), 'embedding', 'cc.en.300.bin')

SAVED_MODELS_PATH = os.path.join(get_project_root(), 'savedmodels')
