import pandas as pd
import numpy as np
import random
from itertools import combinations
from deduplication.base.transformer import Transformer


class TrainPairsCreator(Transformer):
    """
    Selects the pairs that are going to be used for training and validation.
    :param train_ratio: Indicates the ratio between matches and non-matches in the train and validation pairs.
                        e.g. 1 => (match: 50% / non-match: 50%), 2 => (match: 33% / non-match: 67%).
    :param complete: True => returns all possible pairs.
    """

    def __init__(self, train_ratio=1, complete=False):
        self.train_ratio = train_ratio
        self.complete = complete
        super().__init__()

    def fit(self, x):
        return self

    def transform(self, x):
        """
        Construct pairs for training and validation based on the ratio
        :param x: dataset
        :return: pandas DataFrame, with the created and selected pairs
        """

        all_pairs = np.array([
            {'index_1': a,
             'index_2': b}
            for a, b in combinations(x.index, 2)])
        x1 = x.copy()
        x1.rename(columns=lambda p: 'index_1_' + p, inplace=True)
        x2 = x.copy()
        x2.rename(columns=lambda p: 'index_2_' + p, inplace=True)

        if self.complete:
            df = pd.DataFrame([pd.concat([x1.loc[pair['index_1']], x2.loc[pair['index_2']]]) for pair in all_pairs])
            df['label'] = np.where(df['index_1_true_id'] == df['index_2_true_id'], 1, 0)
            return df

        true_pairs = np.array([pair for pair in all_pairs if
                               x.loc[pair['index_1'], 'true_id'] == x.loc[pair['index_2'], 'true_id']])

        false_pairs = random.sample([pair for pair in all_pairs if
                                     x.loc[pair['index_1'], 'true_id'] != x.loc[pair['index_2'], 'true_id']],
                                    len(true_pairs))

        train_pairs = np.concatenate((true_pairs, false_pairs))
        np.random.shuffle(train_pairs)

        df = pd.DataFrame([pd.concat([x1.loc[pair['index_1']], x2.loc[pair['index_2']]]) for pair in train_pairs])
        df['label'] = np.where(df['index_1_true_id'] == df['index_2_true_id'], 1, 0)
        return df
