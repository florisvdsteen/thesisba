from deduplication.base.transformer import Transformer
import pandas as pd
import numpy as np
from itertools import combinations, product
import networkx as nx
from sklearn.metrics.cluster import completeness_score, homogeneity_score, v_measure_score, adjusted_rand_score
from deduplication.blocking.blocking_rules import *


class CompletenessBlocker(Transformer):
    """
    Selects the blocking rules that are going to be used to split the test data. The selection is done based on the
    train set. Then these blocking rules are applied on the test set and the pairs within each of these blocks are
    returned.
    :param attribute_names: Specifies the names of the attributes.
    :param blocking_rules: Specifies the blocking rules that are going to be used. If 'all' it will select all blocking
                           rules that are available.
    :param completeness_threshold: The minimum completeness value that the produced blocks need to meet.
    """

    def __init__(self, attribute_names, blocking_rules='all', completeness_threshold=1.0):

        self.attribute_names = attribute_names
        if blocking_rules == 'all':
            self.blocking_rules = ['complete_attribute', 'first_token', 'all_digits', 'first_three_char',
                                   'first_three_digits']
        else:
            self.blocking_rules = blocking_rules
        self.completeness_threshold = completeness_threshold

        self.rule_1 = self.rule_2 = None
        super().__init__()

    def fit(self, x):
        """
        Decides which combination of blocking rules is most suitable for the dataset. This is done by computing the
        number of comparisons and the completeness scores that each combination of blocking rules produce. The best
        combination of blocking rules is selected by the minimum number of comparisons given the completeness score is
        above the provided threshold.
        :param x: dataset
        :return self
        """
        train_set = x.copy()

        df_d = self.double_rule_metrics(train_set)

        # To give insight
        self.df_display = df_d
        try:
            best_rule_combo = df_d.loc[
                df_d[df_d['completeness'] >= self.completeness_threshold]['number of comparisons'].idxmin()]
        except ValueError:
            raise ValueError('No combination of blocking rules sufficient, please lower Completeness_threshold')

        self.rule_1 = [best_rule_combo['rule 1'].split(' of ')[0], best_rule_combo['rule 1'].split(' of ')[1]]
        self.rule_2 = [best_rule_combo['rule 2'].split(' of ')[0], best_rule_combo['rule 2'].split(' of ')[1]]
        return self

    def transform(self, x):
        """
        Creates pairs from the entity mentions in the test dataset. This is done with the blocking rules that are
        selected by the fit function. Two series of blocks are made based on the two blocking rules. A pair is selected
        if the corresponding entity mentions are within the same block for one of these blocks.
        :param x: train dataset
        :return: Pandas DataFrame, with the created and selected pairs
        """
        x['group_id'] = list(map(eval(self.rule_1[0]), x[self.rule_1[1]]))
        x['group_id2'] = list(map(eval(self.rule_2[0]), x[self.rule_2[1]]))
        all_pairs = np.array([
            {'index_1': a,
             'index_2': b}
            for a, b in combinations(x.index, 2)])
        new_pairs = [pair for pair in all_pairs if
                     x.loc[pair['index_1'], 'group_id'] == x.loc[pair['index_2'], 'group_id'] or x.loc[
                         pair['index_1'], 'group_id2'] == x.loc[pair['index_2'], 'group_id2']]
        x1 = x.copy()
        x1.rename(columns=lambda x: 'index_1_' + x, inplace=True)
        x2 = x.copy()
        x2.rename(columns=lambda x: 'index_2_' + x, inplace=True)
        df = pd.DataFrame([pd.concat([x1.loc[pair['index_1']], x2.loc[pair['index_2']]]) for pair in new_pairs])
        df['label'] = np.where(df['index_1_true_id'] == df['index_2_true_id'], 1, 0)
        return df

    def double_rule_metrics(self, x):
        """
        Computes completeness scores for every combination of two  blocking rules
        :param x: data set
        :return Pandas DataFrame, with for every combination blocking rule the number  of comparisons and the completeness
                score
        """
        all_combinations = list(product(self.blocking_rules, self.attribute_names))
        columns = ['rule 1', 'rule 2', 'number of comparisons', 'completeness']
        df_results = pd.DataFrame(columns=columns)
        for r1, r2 in combinations(all_combinations, 2):
            x['group_id'] = list(map(eval(r1[0]), x[r1[1]]))
            x['group_id2'] = list(map(eval(r2[0]), x[r2[1]]))
            x['group_id_combined'] = [a + b for a, b in
                                      zip(map(eval(r1[0]), x[r1[1]]), map(eval(r2[0]), x[r2[1]]))]
            double_rule(x)
            d = {
                'rule 1': r1[0] + ' of ' + r1[1],
                'rule 2': r2[0] + ' of ' + r2[1],
                'number of comparisons': number_of_comparisons_double(x),
                'completeness': completeness_index(x, 'new_id'),
            }
            df_results = df_results.append(d, ignore_index=True)
        return df_results

    def extensive_metrics(self, x):
        """
        Computes completeness scores for every combination of two  blocking rules
        :param x: data set
        :return Pandas DataFrame, with for every combination blocking rule the number  of comparisons and the completeness
                score
        """
        all_combinations = list(product(self.blocking_rules, self.attribute_names))
        columns = ['rule 1', 'rule 2', 'number of comparisons', 'completeness', 'homogeneity', 'v-measure','adj_rand']
        df_results = pd.DataFrame(columns=columns)
        for r1, r2 in combinations(all_combinations, 2):
            x['group_id'] = list(map(eval(r1[0]), x[r1[1]]))
            x['group_id2'] = list(map(eval(r2[0]), x[r2[1]]))
            x['group_id_combined'] = [a + b for a, b in
                                      zip(map(eval(r1[0]), x[r1[1]]), map(eval(r2[0]), x[r2[1]]))]
            double_rule(x)
            d = {
                'rule 1': r1[0] + ' of ' + r1[1],
                'rule 2': r2[0] + ' of ' + r2[1],
                'number of comparisons': number_of_comparisons_double(x),
                'completeness': completeness_index(x, 'new_id'),
                'homogeneity': homogeneity_index(x, 'new_id'),
                'v-measure': v_measure_index(x, 'new_id'),
                'adj_rand': adj_rand_index(x, 'new_id')
            }
            df_results = df_results.append(d, ignore_index=True)
        return df_results


def double_rule(df):
    g = nx.Graph([(a, b) for a, b in zip(df['group_id'], df['group_id2'])])
    k = list(nx.connected_components(g))

    def return_id(group_id):
        for i in k:
            if group_id in i:
                return k.index(i)

    df['new_id'] = list(map(return_id, df['group_id']))


# metrics
def completeness_index(df, column):
    return completeness_score(df['true_id'], df[column])


def homogeneity_index(df, column):
    return homogeneity_score(df['true_id'], df[column])


def v_measure_index(df, column):
    return v_measure_score(df['true_id'], df[column])

def adj_rand_index(df, column):
    return adjusted_rand_score(df['true_id'], df[column])


def number_of_combinations(size):
    return 0.5 * size * (size - 1)


def number_of_comparisons(df, column):
    return sum(map(number_of_combinations, df.groupby(column).size()))


def number_of_comparisons_double(df):
    sum1 = sum(map(number_of_combinations, df.groupby('group_id').size()))
    sum2 = sum(map(number_of_combinations, df.groupby('group_id2').size()))
    double = sum(map(number_of_combinations, df.groupby('group_id_combined').size()))
    return sum1 + sum2 - double
