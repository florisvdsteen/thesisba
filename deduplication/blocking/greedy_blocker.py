from deduplication.base.transformer import Transformer
import pandas as pd
import numpy as np
from itertools import combinations, product
import networkx as nx
from sklearn.metrics.cluster import completeness_score, homogeneity_score, v_measure_score, adjusted_rand_score
from deduplication.blocking.blocking_rules import *


class GreedyBlocker(Transformer):

    def __init__(self, attribute_names, blocking_rules='all', completeness_threshold=0.98):

        self.attribute_names = attribute_names
        if blocking_rules == 'all':
            self.blocking_rules = ['complete_attribute', 'first_token', 'all_digits', 'first_three_char',
                                   'first_three_digits']
        else:
            self.blocking_rules = blocking_rules
        self.completeness_threshold = completeness_threshold

        self.rules = []
        super().__init__()

    def fit(self, x):
        """
        Decides which  blocking rules are most suitable for the dataset. This is done by greedily selecting the best
        blocking rule, i.e. the blocking rule with the minimum amount of comparisons with the condition that the
        completeness score is still above the threshold. This process keeps repeating until adding an available
        blocking rule would not satisfy the threshold.
        :param x: dataset
        :return self
        """
        train_set = x.copy()

        threshold_not_reached = True
        while threshold_not_reached:

            # computes dataframe with completeness scores and number of comparisons for every new blocking rule in
            # combination with the already selected blocking rules.
            df_d, train_set = self.compute_rules_metrics(train_set)
            print(df_d)
            if max(df_d['completeness']) < self.completeness_threshold:
                threshold_not_reached = False
            else:
                # selects the best rule to add
                best_rule = df_d.loc[
                    df_d[df_d['completeness'] >= self.completeness_threshold]['number of comparisons'].idxmin()]
                self.rules.append([best_rule['rule'], best_rule['attribute']])
        return self

    def transform(self, x):
        """
        Blocker that greedily selects the blocking rule that minimizes the amount of comparisons that needs to be made.
        This is done until a given 'quality' threshold is reached. The completeness score is used to compute this
        quality. Is best used for large datasets, downside are the 'hard' blocks. Entity mentions can not get any
        connections outside of the blocks.
        :param x: train dataset
        :return: Pandas DataFrame, with the created and selected pairs
        """
        # group id based on the selected blocking rules
        x['group_id'] = [''.join(k) for k in zip(*[np.char.array(list(map(eval(r[0]), x[r[1]]))) for r in self.rules])]

        new_pairs = np.concatenate(
            [np.array([{'index_1': a, 'index_2': b} for a, b in combinations(group.index, 2)]) for _, group in
             x.groupby('group_id')])

        x1 = x.copy()
        x1.rename(columns=lambda x: 'index_1_' + x, inplace=True)
        x2 = x.copy()
        x2.rename(columns=lambda x: 'index_2_' + x, inplace=True)
        df = pd.DataFrame([pd.concat([x1.loc[pair['index_1']], x2.loc[pair['index_2']]]) for pair in new_pairs])
        df['label'] = np.where(df['index_1_true_id'] == df['index_2_true_id'], 1, 0)
        return df

    def compute_rules_metrics(self, x):
        """
        Computes completeness scores and number of comparisons for every new blocking rule in combination with the
        already selected blocking rules.
        :param x: data set
        :return Pandas DataFrame, with for every new blocking rule the number of comparisons and the completeness score
        """
        columns = ['attribute', 'rule', 'number of comparisons', 'completeness']
        df_results = pd.DataFrame(columns=columns)
        for attribute in self.attribute_names:
            for rule in self.blocking_rules:
                if [rule, attribute] not in self.rules:
                    # new blocking rule group id
                    new_group_id_component = np.char.array(list(map(eval(rule), x[attribute])))

                    if len(self.rules) > 0:
                        # group id of already selected blocking rules
                        old_group_id_component = [''.join(k) for k in zip(
                            *[np.char.array(list(map(eval(r[0]), x[r[1]]))) for r in self.rules])]
                    else:
                        old_group_id_component = ''
                    # new key, combination of the existing blocking rules and the new blocking rule of the loop
                    x['group_id'] = new_group_id_component + old_group_id_component

                    d = {
                        'attribute': attribute,
                        'rule': rule,
                        'number of comparisons': number_of_comparisons(x, 'group_id'),
                        'completeness': completeness_index(x, 'group_id')
                    }
                    df_results = df_results.append(d, ignore_index=True)
        return df_results, x

    def compute_metrics(self, x):
        """
        Computes completeness scores and number of comparisons for every new blocking rule in combination with the
        already selected blocking rules.
        :param x: data set
        :return Pandas DataFrame, with for every new blocking rule the number of comparisons and the completeness score
        """
        columns = ['number of comparisons', 'completeness', 'homogeneity', 'v-measure', 'adj_rand']
        df_results = pd.DataFrame(columns=columns)

        if len(self.rules) > 0:
            # group id of already selected blocking rules
            old_group_id_component = [''.join(k) for k in zip(
                *[np.char.array(list(map(eval(r[0]), x[r[1]]))) for r in self.rules])]
        else:
            old_group_id_component = ''
        # new key, combination of the existing blocking rules and the new blocking rule of the loop
        x['group_id'] = old_group_id_component

        d = {
            'number of comparisons': number_of_comparisons(x, 'group_id'),
            'completeness': completeness_index(x, 'group_id'),
            'homogeneity': homogeneity_index(x, 'group_id'),
            'v-measure': v_measure_index(x, 'group_id'),
            'adj_rand': adj_rand_index(x, 'group_id')
        }
        df_results = df_results.append(d, ignore_index=True)

        return df_results


# metrics
def completeness_index(df, column):
    return completeness_score(df['true_id'], df[column])


def homogeneity_index(df, column):
    return homogeneity_score(df['true_id'], df[column])


def v_measure_index(df, column):
    return v_measure_score(df['true_id'], df[column])


def adj_rand_index(df, column):
    return adjusted_rand_score(df['true_id'], df[column])


def number_of_combinations(size):
    return 0.5 * size * (size - 1)


def number_of_comparisons(df, column):
    return sum(map(number_of_combinations, df.groupby(column).size()))
