import string

__all__ = ['complete_attribute', 'first_token', 'all_digits', 'first_three_char',
           'first_three_digits']


# blocking rules:
def complete_attribute(s):
    return str(s)


def first_token(s):
    return str(s).split(' ')[0]


def all_digits(s):
    extract_digits = lambda x: "".join(char for char in x if char in string.digits)
    return extract_digits(str(s))


def first_three_char(s):
    return str(s)[:3]


def first_three_digits(s):
    extract_digits = lambda x: "".join(char for char in x if char in string.digits)[:3]
    return extract_digits(str(s))


def last_three_char(s):
    return str(s)[:-3]
