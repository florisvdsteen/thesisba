from unidecode import unidecode
import re
import csv
from sklearn.model_selection import train_test_split
import pandas as pd
import numpy as np
import fasttext


def pre_process(column):
    """
    Do a little bit of data cleaning with the help of Unidecode and Regex.
    Things like casing, extra spaces, quotes and new lines can be ignored.
    """
    column = unidecode(column)
    column = re.sub('  +', ' ', column)
    column = re.sub('\n', ' ', column)
    column = column.strip().strip('"').strip("'").lower().strip()
    # If data is missing, indicate that by setting the value to `None`
    if not column:
        column = ''
    return column


def read_data(filename):
    """
    Read in our data from a CSV file and create a dictionary of records,
    where the key is a unique record ID and each value is dict
    """
    data_d = {}
    with open(filename) as f:
        reader = csv.DictReader(f)
        for row in reader:
            clean_row = [(k, pre_process(v)) for (k, v) in row.items()]
            row_id = int(row['Id'])
            data_d[row_id] = dict(clean_row)

    return data_d


def simplify_string(df):
    for string in df['data_strings'].to_numpy():
        print(string)
        string_split = [token for token in string.split(" ") if len(token) > 1]
        print(string_split)
        new_string = ' '.join([re.sub('[^A-Za-z0-9]+', '', string) for string in string_split])
        df.replace(string, new_string)
    return df


def subset_data(df, size):
    np.random.seed(50)
    unique_id = pd.DataFrame(df['true_id'].unique())
    subset_1, subset_2 = np.split(unique_id.sample(frac=1, random_state=10), [int(size * len(unique_id))])
    return df[df['true_id'].isin(subset_1[0].to_numpy())]


def column_to_string(df, columns):
    for column in columns:
        df[column] = df[column].apply(str)


def trainvaltest_split(df_entity_mentions, val_size=0.05, test_size=0.05):
    """

    :param data_vector: list of dictionaries with 'data_string' and 'label' for each entities
    :param test_size: size of the test set
    :return: train_set and test_set
    """

    np.random.seed(50)
    unique_id = pd.DataFrame(df_entity_mentions['true_id'].unique())
    train, validate, test = np.split(unique_id.sample(frac=1, random_state=10),
                                     [int((1 - val_size - test_size) * len(unique_id)),
                                      int((1 - test_size) * len(unique_id))])

    return df_entity_mentions[df_entity_mentions['true_id'].isin(train[0].to_numpy())], \
           df_entity_mentions[df_entity_mentions['true_id'].isin(validate[0].to_numpy())], \
           df_entity_mentions[df_entity_mentions['true_id'].isin(test[0].to_numpy())]


def lamda(args):
    pass


def build_distributions(df_mentions, columns):
    df_mentions['total_tokens'] = np.zeros(len(df_mentions))
    df_mentions['total_characters'] = np.zeros(len(df_mentions))
    for att in columns:
        df_mentions['characters_' + att] = list(map(len, df_mentions[att]))
        df_mentions['tokens_' + att] = list(map(lambda x: len(x.split()), df_mentions[att]))
        df_mentions['total_characters'] += df_mentions['characters_' + att]
        df_mentions['total_tokens'] += df_mentions['tokens_' + att]

    return df_mentions


def reading_data_for_exploration(path_data, path_labels):
    data = read_data(path_data)
    data_labels = read_data(path_labels)
    df_mentions = pd.DataFrame(
        {  # 'data_strings': [' '.join([data[key][i['field']] for i in fields]) for key in data],
            'site_name': [data[key]['Site name'] for key in data],
            'address': [data[key]['Address'] for key in data],
            'zip': [data[key]['Zip'] for key in data],
            'phone': [data[key]['Phone'] for key in data],
            'true_id': [data_labels[int(data[key]['Id'])]['True Id'] for key in data]
        }
    )
    df_mentions.reset_index(inplace=True)
    columns = ['site_name', 'address', 'zip']
    return build_distributions(df_mentions, columns)


def data_preparation(path_data, path_labels, val_size, test_size, attribute_structure=None):
    """

    :param path_data: path to folder containing csv file containing data
    :param path_labels: path to folder containing csv file with labels
    :param test_size: size of the test set
    :return: train_set and test_set: list of dictionaries with 'data_string' and 'label'
    """
    data = read_data(path_data)
    data_labels = read_data(path_labels)

    df_mentions = pd.DataFrame(
        {  # 'data_strings': [' '.join([data[key][i['field']] for i in fields]) for key in data],
            'site_name': [data[key]['Site name'] for key in data],
            'address': [data[key]['Address'] for key in data],
            'zip': [data[key]['Zip'] for key in data],
            'phone': [data[key]['Phone'] for key in data],
            'true_id': [data_labels[int(data[key]['Id'])]['True Id'] for key in data]
        }

    )
    df_mentions.reset_index(inplace=True)

    train_set, val_set, test_set = trainvaltest_split(df_mentions, val_size=val_size, test_size=test_size)

    return train_set, val_set, test_set


def transform_pairs(df_pairs, proportion=1):
    matching_pairs = df_pairs[df_pairs['label'] == 1]
    non_matching_pairs = df_pairs[df_pairs['label'] == 0]

    return pd.concat([matching_pairs, non_matching_pairs.sample(proportion * len(matching_pairs))]).sample(frac=1)
