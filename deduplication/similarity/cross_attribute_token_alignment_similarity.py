import os

import numpy as np

from tensorflow.keras.layers import Multiply, Dense, Lambda, Average, Concatenate, Softmax, GRU, Input, Bidirectional
from tensorflow.keras.models import Model as KerasModel
from tensorflow.keras.callbacks import EarlyStopping
from tensorflow.keras.optimizers import Adam
import tensorflow.keras.backend as K
from tensorflow.keras.models import load_model

from deduplication.base.model import Model


def compare_matrix(h):
    return [K.abs(left - h[1]) for left in h[0]]


class CrossAttributeTokenAlignment(Model):
    """
    Model for computing a similarity score between two entity mentions. The model utilizes FastText word embeddings and
    a cross attribute technique to find similarity. The model uses GRU units to incorporate contextual information and
    tries to match tokens across all of the attributes.
    :param attribute_names: Specifies the names of the attributes.
    :param amount_attribute_tokens: The number of tokens uses for each attribute. If not specified all attribute will
                                    use 6 tokens.
    :param input_length: Size of embedded input from Fasttext. Default = 300.
    :param gru_dim_size: Dimension of the output space of the GRU layer. Default = 50.
    :param dense_dim_size: Dimension of the output space of the Dense layers. Default = 100.
    :param batch_size: Number of samples per gradient update. Default = 32.
    :param n_epochs: Number of epochs to train the model. Default = 500.
    :param learning_rate: Learning rate for the Adam optimizer. Default = 0.001.
    :param patience: Number of epochs with no improvement after which training will be stopped. Default = 50.
    """

    def __init__(self, attribute_names, amount_attribute_tokens=None, input_length=300, gru_dim_size=50,
                 dense_dim_size=100, batch_size=32, n_epochs=500, learning_rate=0.001, patience=50):

        self.attribute_names = attribute_names
        self.number_of_attributes = len(self.attribute_names)
        if amount_attribute_tokens is None:
            self.amount_attribute_tokens = [6 for _ in range(self.number_of_attributes)]
        else:
            self.amount_attribute_tokens = amount_attribute_tokens

        self.total_number_of_tokens = sum(self.amount_attribute_tokens)

        self.input_length = input_length
        self.gru_dim_size = gru_dim_size
        self.dense_dim_size = dense_dim_size
        self.batch_size = batch_size
        self.n_epochs = n_epochs
        self.learning_rate = learning_rate
        self.patience = patience

        self.name = f"CATA_model_" \
                    f"gru={self.gru_dim_size}_dense={self.dense_dim_size}_b={self.batch_size}_lr={self.learning_rate}"

        self.cata_model = self.history = None
        super().__init__()

    def create_model(self):
        """
        Creates the model based on the attribute structure and the provided parameters
        """
        l_inputs = [Input(shape=(self.input_length, 1)) for i in range(self.total_number_of_tokens)]
        r_inputs = [Input(shape=(self.input_length, 1)) for i in range(self.total_number_of_tokens)]

        contextual_gru_layer = Bidirectional(GRU(self.gru_dim_size, name='GRU_layer'))
        l_output_gru = [contextual_gru_layer(i) for i in l_inputs]
        r_output_gru = [contextual_gru_layer(i) for i in r_inputs]

        comparison_matrix_function = Lambda(compare_matrix, name="Comparison_lambda_layer")
        comparison_matrix = comparison_matrix_function([l_output_gru, r_output_gru])

        comparison_linear_layer = Dense(1, name='Comparison_linear_layer')
        output_comp_lin = [comparison_linear_layer(c) for c in comparison_matrix]

        comparison_softmax_layer = Softmax(axis=0, name='Comparison_softmax_layer')
        attention_vectors = [comparison_softmax_layer(g) for g in output_comp_lin]

        mult_prob_layer = Multiply(name='product_comp_matrix_and_probs')
        token_prob_mult = [K.sum(mult_prob_layer([c, a]), axis=0) for c, a in
                           zip(comparison_matrix, attention_vectors)]

        cum_atr = np.cumsum([0] + self.amount_attribute_tokens)
        attribute_matching_vectors = [Average()(token_prob_mult[cum_atr[i - 1]:cum_atr[i]]) if
                                      len(token_prob_mult[cum_atr[i - 1]:cum_atr[i]]) > 1 else
                                      token_prob_mult[cum_atr[i - 1]:cum_atr[i]][0]
                                      for i in range(1, len(cum_atr))]
        if self.number_of_attributes > 1:
            final_matching_vector = Concatenate(axis=-1)(attribute_matching_vectors)

        else:
            final_matching_vector = attribute_matching_vectors[0]

        prediction_relu_layer = Dense(self.dense_dim_size, activation='relu', name='Prediction_relu_layer')(
            final_matching_vector)

        match = Dense(1, activation='sigmoid')(prediction_relu_layer)

        self.cata_model = KerasModel(inputs=[l_inputs, r_inputs], outputs=[match])

        self.cata_model.compile(loss='binary_crossentropy', optimizer=Adam(learning_rate=0.01), metrics=['accuracy'])

    def fit(self, x_train, x_val=None):
        """
        Trains the CATA model, this method transforms the input (x_train and x_val) into the suitable format, specifies
        the callbacks and calls the Keras.model.fit function of the CATA model
        :param x_train: selected pairs from the train set
        :param x_val: selected pairs from the validation set
        :param verbose: The level of logging information
        :return self
        """

        self.create_model()

        columns_1 = ['index_1_embedded_' + a for a in self.attribute_names]
        columns_2 = ['index_2_embedded_' + a for a in self.attribute_names]

        entity_1_train = list(
            np.concatenate([np.transpose(list(x_train[column]), axes=(1, 0, 2)) for column in columns_1]))
        entity_2_train = list(
            np.concatenate([np.transpose(list(x_train[column]), axes=(1, 0, 2)) for column in columns_2]))
        entity_1_val = list(np.concatenate([np.transpose(list(x_val[column]), axes=(1, 0, 2)) for column in columns_1]))
        entity_2_val = list(np.concatenate([np.transpose(list(x_val[column]), axes=(1, 0, 2)) for column in columns_2]))

        callbacks = EarlyStopping(patience=self.patience, restore_best_weights=True)
        self.history = self.cata_model.fit([entity_1_train, entity_2_train], x_train['label'].to_numpy(),
                            validation_data=([entity_1_val, entity_2_val], x_val['label'].to_numpy()),
                            batch_size=self.batch_size, epochs=self.n_epochs, callbacks=callbacks)
        return self

    def predict(self, x):
        """
         Computes the similarity scores of the test dataset with the Keras.model.predict function of the CATA model.
         :param x: selected pairs from the test dataset.
         :return Pandas DataFrame, with the same structure as the input but with an extra column containing the
                 similarity scores for the corresponding pairs.
         """

        columns_1 = ['index_1_embedded_' + a for a in self.attribute_names]
        columns_2 = ['index_2_embedded_' + a for a in self.attribute_names]
        entity_1_test = list(np.concatenate([np.transpose(list(x[column]), axes=(1, 0, 2)) for column in columns_1]))
        entity_2_test = list(np.concatenate([np.transpose(list(x[column]), axes=(1, 0, 2)) for column in columns_2]))
        x['similarity_score'] = self.cata_model.predict([entity_1_test, entity_2_test])
        return x

    def save_model(self, path):
        self.cata_model.save(os.path.join(path, self.name))

    def load_model(self, path):
        self.cata_model = load_model(os.path.join(path, self.name))

    def create_fasttext_embeddings(self, ft, df):
        """
        Constructs fasttext token embeddings for a dataset
        :param ft: fasttext model
        :param df: dataset that needs embeddings
        """
        for attribute, max_token_count in zip(self.attribute_names, self.amount_attribute_tokens):
            df['embedded_' + attribute] = [attribute_string_to_list_of_embeddings(ft, string, max_token_count) for
                                           string in df[attribute]]
        return df


def attribute_string_to_list_of_embeddings(ft, string, max_token_count):
    """
    Constructs Fasttext token embeddings for an attribute string. The embedding vector is padded or truncated to achieve
    the desired amount of tokens.
    :param ft: Fasttext model
    :param string: Attribute that needs embeddings
    :param max_token_count: Number of tokens that need embeddings
    :return ndarray, with length max_token_count containing lists of 300 values corresponding to the token embeddings
    """
    embeddings_vectors = [ft.get_word_vector(token) for token in string.split(" ") if len(token) > 1][:max_token_count]
    while len(embeddings_vectors) < max_token_count:
        embeddings_vectors.append(100 * np.random.rand(300))
    return np.array(embeddings_vectors)
