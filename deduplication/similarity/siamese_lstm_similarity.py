import string
import os
import numpy as np
from functools import reduce

from tensorflow.keras.callbacks import EarlyStopping
from tensorflow.keras.layers import Embedding, Input, LSTM, Lambda, Bidirectional
import tensorflow.keras.backend as K
from deduplication.base.model import Model
from tensorflow.keras.preprocessing.text import Tokenizer
from tensorflow.keras.preprocessing.sequence import pad_sequences
from tensorflow.keras.models import Model as KerasModel
from tensorflow.keras.models import load_model
from tensorflow.keras.optimizers import Adam


def neg_exp_man_dist(left, right):
    return K.exp(-K.sum(K.abs(left - right), keepdims=True, axis=1))


class SiameseLSTM(Model):
    """
    Model for computing a similarity score between two entity mentions. The model utilizes a siamese LSTM network to
    find similarity.
    :param attribute_names: Specifies the names of the attributes.
    :param input_size: The amount of characters from the data string that are going to be used in the model.
                        Default = 125.
    :param emb_dim: Dimension of the output space of the embedding layer. Default = 24.
    :param lstm_dim: Dimension of the output space of the LSTM layer. Default = 100.
    :param batch_size: Number of samples per gradient update. Default = 32.
    :param n_epochs: Number of epochs to train the model. Default = 500.
    :param learning_rate: Learning rate for the Adam optimizer. Default = 0.001.
    :param patience: Number of epochs with no improvement after which training will be stopped. Default = 50.
    :param vocabulary: Text corpus used for building the character dictionary. Default = all known characters defined
                       by string.printable.
    """

    def __init__(self, attribute_names, input_size=125, emb_dim=24, lstm_dim=100, batch_size=32, n_epochs=500,
                 learning_rate=0.001, patience=50, vocabulary=string.printable):
        self.attribute_names = attribute_names
        self.vocabulary = vocabulary

        self.input_size = input_size
        self.emb_dim = emb_dim
        self.lstm_dim = lstm_dim
        self.batch_size = batch_size
        self.n_epochs = n_epochs
        self.learning_rate = learning_rate
        self.patience = patience

        self.name = f"SiameseLSTM_model_" \
                    f"emb={self.emb_dim}_lstm={self.lstm_dim}_b={self.batch_size}_lr={self.learning_rate}"

        self.tk = self.lstm_model = self.history = None
        super().__init__()

    def create_model(self):
        """
        Creates the model based on the attribute structure and the provided parameters
        """

        self.tk = Tokenizer(split='', char_level=True)
        self.tk.fit_on_texts(self.vocabulary)
        vocab_size = len(self.tk.word_index) + 1

        l_input = Input(shape=(self.input_size,))
        r_input = Input(shape=(self.input_size,))

        embedding = Embedding(vocab_size, self.emb_dim, mask_zero=True)
        l_encoded = embedding(l_input)
        r_encoded = embedding(r_input)
        shared_lstm = Bidirectional(LSTM(self.lstm_dim,recurrent_initializer='glorot_uniform'))

        l_lstm = shared_lstm(l_encoded)
        r_lstm = shared_lstm(r_encoded)

        similarity_function = Lambda(function=lambda x: neg_exp_man_dist(x[0], x[1]),
                                     output_shape=lambda x: (x[0][0], 1))([l_lstm, r_lstm])

        self.lstm_model = KerasModel(inputs=[l_input, r_input], outputs=[similarity_function])

        self.lstm_model.compile(loss='mean_squared_error', optimizer=Adam(learning_rate=self.learning_rate),
                                metrics=['accuracy'])

    def fit(self, x_train, x_val=None):
        """
        Trains the LSTM model, this method transforms the input (x_train and x_val) into the suitable format, specifies
        the callbacks and calls the Keras.model.fit function of the LSTM model.
        :param x_train: selected pairs from the train set.
        :param x_val: selected pairs from the validation set.
        :param verbose: The level of logging information.
        :return self
        """

        self.create_model()

        # concatenate relevant attributes into a single data string
        train_1_strings = reduce(lambda a, b: a + ' ' + b,
                                 [x_train["index_1_" + attribute] for attribute in self.attribute_names])
        train_2_strings = reduce(lambda a, b: a + ' ' + b,
                                 [x_train["index_2_" + attribute] for attribute in self.attribute_names])
        validation_1_strings = reduce(lambda a, b: a + ' ' + b,
                                      [x_val["index_1_" + attribute] for attribute in self.attribute_names])
        validation_2_strings = reduce(lambda a, b: a + ' ' + b,
                                      [x_val["index_2_" + attribute] for attribute in self.attribute_names])

        # character based embedding
        train_1_seq = self._string_to_sequence(train_1_strings)
        train_2_seq = self._string_to_sequence(train_2_strings)
        validation_1_seq = self._string_to_sequence(validation_1_strings)
        validation_2_seq = self._string_to_sequence(validation_2_strings)

        callbacks = EarlyStopping(patience=self.patience, restore_best_weights=True)
        self.history = self.lstm_model.fit([np.array(train_1_seq), np.array(train_2_seq)], x_train['label'].to_numpy(),
                            validation_data=(
                                [np.array(validation_1_seq), np.array(validation_2_seq)], x_val['label'].to_numpy()),
                            batch_size=self.batch_size, epochs=self.n_epochs, callbacks=callbacks)
        return self

    def predict(self, x):
        """
         Computes the similarity scores of the test dataset with the Keras.model.predict function of the LSTM model.
         :param x: selected pairs from the test dataset.
         :return Pandas DataFrame, with the same structure as the input but with an extra column containing the
                 similarity scores for the corresponding pairs.
         """

        test_1_strings = reduce(lambda a, b: a + ' ' + b,
                                [x["index_1_" + attribute] for attribute in self.attribute_names])
        test_2_strings = reduce(lambda a, b: a + ' ' + b,
                                [x["index_2_" + attribute] for attribute in self.attribute_names])
        entity_1_seq = self._string_to_sequence(test_1_strings)
        entity_2_seq = self._string_to_sequence(test_2_strings)
        x['similarity_score'] = self.lstm_model.predict([np.array(entity_1_seq), np.array(entity_2_seq)])
        return x

    def save_model(self, path):
        self.lstm_model.save(os.path.join(path, self.name))

    def load_model(self, path):
        self.lstm_model = load_model(os.path.join(path, self.name))

    def _string_to_sequence(self, x):
        return pad_sequences(self.tk.texts_to_sequences(x), maxlen=self.input_size, padding='post')
