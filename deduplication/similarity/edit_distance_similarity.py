import numpy as np
from functools import reduce
from textdistance import levenshtein, hamming, jaro_winkler
from deduplication.base.transformer import Transformer


class EditDistanceSimilarity(Transformer):
    """
    Model for computing a similarity score between two entity mentions.This is done by a token based comparison with
    a suitable string metric (the Levenshtein/Hamming/Jaro-Winkler distance).
    :param attribute_names: Specifies the names of the attributes.
    :param similarity_algorithm: Algorithm used to determine the similarity between token strings between entity record
                                 mentions. Default = 'Jaro-Winkler'.
    :param similarity_threshold: Threshold dat determines whether or not token strings are considered to be similar.
                                 Default = 0.95.
    """

    def __init__(self, attribute_names, similarity_algorithm='Jaro-Winkler', similarity_threshold=0.95):
        self.attribute_names = attribute_names
        self.similarity_algorithm = similarity_algorithm
        self.similarity_threshold = similarity_threshold
        super().__init__()

    def fit(self, x):
        return self

    def transform(self, x):
        """
         Computes the similarity scores of the test dataset with the help of the selected string metric.
         :param x: selected pairs from the test dataset
         :return Pandas DataFrame, with the same structure as the input but with an extra column containing the
                 similarity scores for the corresponding pairs
         """
        test_1_strings = reduce(lambda a, b: a + ' ' + b,
                                [x["index_1_" + attribute] for attribute in self.attribute_names])
        test_2_strings = reduce(lambda a, b: a + ' ' + b,
                                [x["index_2_" + attribute] for attribute in self.attribute_names])

        similarity_scores = [self._compute_similarity(e_1, e_2) for e_1, e_2 in zip(test_1_strings, test_2_strings)]
        x['similarity_score'] = similarity_scores
        return x

    def _edit_distance(self, token_1, token_2):
        if self.similarity_algorithm == 'Levenshtein':
            return levenshtein.normalized_similarity(token_1, token_2)

        if self.similarity_algorithm == 'Hamming':
            return hamming.normalized_similarity(token_1, token_2)

        if self.similarity_algorithm == 'Jaro-Winkler':
            return jaro_winkler(token_1, token_2)
        print('No algorithm named: ' + self.similarity_algorithm)
        return 0

    def _compute_similarity(self, entity_1, entity_2):
        """
        Computes similarity by token based comparison with the Levenshtein/Hamming/Jaro-Winkler distance. A token
        comparison matrix (n x m) is constructed with n: number of tokens entity 1 and m: number of tokens entity 2.
        matrix(i,j) contains the Levenshtein/Hamming/Jaro-Winkler distance ratio between token i and token j. Then for
        each token the maximum value is picked which gives the highest similarity with the tokens of the other entity.
        This is compared with a threshold (default = 0.95). Then the final similarity score is calculated by the amount
        of entity matches above threshold for entity1 and entity2 divided by the total number of tokens.
        :param entity_1: data string of entity 1
        :param entity_2: data string of entity 2
        :return similarity score between entity 1 and entity 2
        """

        entity_1_tokens = tokenize(entity_1)
        size_entity_1 = len(entity_1_tokens)
        entity_2_tokens = tokenize(entity_2)
        size_entity_2 = len(entity_2_tokens)

        comparison_matrix = np.zeros((size_entity_1, size_entity_2))
        for i in range(size_entity_1):
            for j in range(size_entity_2):
                comparison_matrix[i][j] = self._edit_distance(entity_1_tokens[i], entity_2_tokens[j])

        entity_1_token_similarity = comparison_matrix.max(axis=1)
        entity_2_token_similarity = comparison_matrix.max(axis=0)
        return (np.sum(entity_1_token_similarity > self.similarity_threshold) + np.sum(
            entity_2_token_similarity > self.similarity_threshold)) / (size_entity_1 + size_entity_2)


def tokenize(data_string):
    """
    :param data_string: string with entity data
    :return: list of tokens present in the data string
    """
    return [token for token in data_string.split(" ") if len(token) > 1]
