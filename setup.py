from setuptools import setup, find_packages

setup(
    name='Deduplication',
    author='Floris van der Steen',
    version='1.0',
    packages=find_packages(include=['deduplication']),
    install_requires=['scikit-learn', 'tensorflow', 'networkx', 'textdistance', 'fasttext', 'pandas', 'numpy', 'scipy']
)
